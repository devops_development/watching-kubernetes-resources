package main

import (
    "context"
    "os"
    "sync"

    log "github.com/sirupsen/logrus"
    corev1 "k8s.io/api/core/v1"
    rbacv1 "k8s.io/api/rbac/v1"
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    "k8s.io/apimachinery/pkg/watch"
    "k8s.io/client-go/kubernetes"
    "k8s.io/client-go/tools/cache"
    "k8s.io/client-go/tools/clientcmd"
    toolsWatch "k8s.io/client-go/tools/watch"
)

var (
    config, _    = clientcmd.BuildConfigFromFlags("", os.Getenv("KUBECONFIG"))
    clientset, _ = kubernetes.NewForConfig(config)
)

func watchNamespaces() {

    watchFunc := func(options metav1.ListOptions) (watch.Interface, error) {
        timeOut := int64(60)
        return clientset.CoreV1().Namespaces().Watch(context.Background(), metav1.ListOptions{TimeoutSeconds: &timeOut})
    }

    watcher, _ := toolsWatch.NewRetryWatcher("1", &cache.ListWatch{WatchFunc: watchFunc})

    for event := range watcher.ResultChan() {
        item := event.Object.(*corev1.Namespace)

        switch event.Type {
        case watch.Modified:
        case watch.Bookmark:
        case watch.Error:
        case watch.Deleted:
        case watch.Added:
            processNamespace(item.GetName())
        }
    }
}

func processNamespace(namespace string) {
    log.Info("Some processing for newly created namespace : ", namespace)

    clientset.RbacV1().Roles(namespace).Update(context.Background(), getRole(namespace), metav1.UpdateOptions{})
    clientset.RbacV1().RoleBindings(namespace).Update(context.Background(), getRoleBinding(namespace), metav1.UpdateOptions{})
}

func getRole(namespace string) *rbacv1.Role {
    return &rbacv1.Role{
        ObjectMeta: metav1.ObjectMeta{
            Name: namespace + "-dev",
        },
        Rules: []rbacv1.PolicyRule{
            {
                APIGroups: []string{rbacv1.APIGroupAll},
                Resources: []string{rbacv1.ResourceAll},
                Verbs:     []string{rbacv1.VerbAll},
            },
        },
    }
}

func getRoleBinding(namespace string) *rbacv1.RoleBinding {
    return &rbacv1.RoleBinding{
        ObjectMeta: metav1.ObjectMeta{
            Name: namespace + "-dev",
        },
        RoleRef: rbacv1.RoleRef{
            APIGroup: "",
            Kind:     "Role",
            Name:     namespace + "-dev",
        },
// Those users should be fetched either from internal cm or some external storage
        Subjects: []rbacv1.Subject{
            {
                APIGroup: "rbac.authorization.k8s.io",
                Kind:     "User",
                Name:     "devopstales",
            },
            {
                APIGroup: "rbac.authorization.k8s.io",
                Kind:     "User",
                Name:     "user2@tenant1.com",
            },
        },
    }
}

func main() {
    var wg sync.WaitGroup
    go watchNamespaces()
    wg.Add(1)
    wg.Wait()
}