for VARIABLE in default kube-system kube-public kube-node-lease cloudflare cvastro longhorn-system tenantns asusns minio-operator minio-tenant-1 monitoring

do
   echo Role/$VARIABLE-dev -n $VARIABLE
   kubectl delete  Role/$VARIABLE-dev -n $VARIABLE   
   kubectl delete rolebinding $VARIABLE-dev -n $VARIABLE 

done   