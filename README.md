# watching-kubernetes-resources

Based on https://blog.mimacom.com/k8s-watch-resources/


```bash
go mod init watching-kubernetes-resources

export KUBECONFIG="/home/juno/.kube/config"

k get role test-dev -A

docker buildx build -t "remotejob/k8s-events-demo:0.1" --platform linux/amd64,linux/arm64 --push .
k apply -f deploymentprod.yaml
k delete -f deploymentprod.yam```